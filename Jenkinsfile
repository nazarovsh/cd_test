def versionNumber
def buildTimestamp
def applicationName
def registryPath = 'http://192.168.1.46:30000'
def registryCredential = 'docker-registry-admin'
def dockerImage

pipeline {

    agent any

    stages {

        stage('Cleaning') {
            steps {
               sh './gradlew clean'
            }
        }

        stage('Analyse/Test/Build') {
            steps {
               sh './gradlew test'
               sh './gradlew build'
            }
        }

        stage('Versioning') {
            steps {
                script {
                            def props = readProperties  file:'build/build-info.properties'
                            versionNumber = props['version']
                            buildTimestamp = props['timestamp']
                            applicationName = props['applicationName']
                            echo "Version Number = ${versionNumber}"
                            echo "Build Timestamp=${buildTimestamp}"
                            echo "Application Name=${applicationName}"
                            echo "Manufacturer=${applicationName}"
                }
            }
        }

        stage('Docker Publish') {
            steps {
              script {
                def imageWithVersion = applicationName+":"+versionNumber
                dockerImage = docker.build imageWithVersion
                docker.withRegistry(registryPath, registryCredential) {
                      dockerImage.push(versionNumber)
                      dockerImage.push("latest")
                }
              }
            }
        }

        stage('Docker Deploy') {
            steps {
              script {
              def workspacePath = pwd()
              sshPublisher(
                 continueOnError: false, failOnError: true,
                 publishers: [
                  sshPublisherDesc(
                   configName: "test_apps_vm",
                   verbose: true,
                   transfers: [
                    sshTransfer(
                     execCommand: "bash scripts/start-bitbucket.sh"
                                )
                   ])
                 ])
              }
            }
        }


    }

    post {
            always {
                echo 'Sending Email'

                emailext body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${applicationName} version: ${versionNumber}\n More info at: ${env.BUILD_URL}",
                    recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                    subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"

            }
    }
}