package com.shahinnazarov.devops.bitbucket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright Shahin Nazarov<me@shahinnazarov.com>
 */
@RestController
@PropertySource("classpath:build-info.properties")
public class MainController {

    @Value("${applicationName}")
    private String applicationName;
    @Value("${version}")
    private String versionNumber;
    @Value("${timestamp}")
    private String buildTimestamp;


    @GetMapping
    public String helloWorld() {
        return String.format("%s version: %s was built at %s", applicationName, versionNumber, buildTimestamp);
    }
}
